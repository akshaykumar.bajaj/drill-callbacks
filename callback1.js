/* 
    Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in
     boards.json and then pass control back to the code that called it by using a callback function.
*/
const fs = require('fs');

module.exports = function callback1(boardIDs, callback, filepath) {
    setTimeout(() => {
        if (typeof callback !== 'function') {
            console.error(new Error('callback parameter should be a function!'));
        }
        else if (typeof boardIDs !== 'string' && !Array.isArray(boardIDs)) {
            callback(new Error('boardID parameter should be a string!'));
        }
        else if (typeof filepath !== 'string') {
            callback(new Error('filepath parameter should be a string!'));
        } else {

            fs.readFile(filepath, 'utf-8', (readError, boardsJSON) => {
                let errorMessage;

                if (readError) {
                    errorMessage = readError;
                }
                else {
                    try {

                        let boards = JSON.parse(boardsJSON);
                        let boardInfo;

                        if (typeof boardIDs === 'string') {
                            boardInfo = boards.filter(board => board.id === boardIDs)
                        }
                        else {
                            boardInfo = boardIDs.map(boardID => boards.filter(board => board.id === boardID));
                        }
                        if (boardInfo.length === 0) {
                            errorMessage = new Error('Board Not Found!');
                        } else {
                            callback(null, boardInfo);
                        }
                    }
                    catch (parseError) {
                        errorMessage = parseError;
                    }
                }
                if (errorMessage != undefined) {
                    callback(errorMessage);
                }
            });
        }
    }, 2 * 1000);
}