/* 
    Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it ,from the given data in lists.json.
    Then pass control back to the code that called it by using a callback function.
*/
const fs = require('fs');

module.exports = function callback2(boardID, callback, filepath) {
    setTimeout(() => {
        if (typeof callback !== 'function') {
            console.error(new Error('callback parameter should be a function!'));
        }
        else if (typeof boardID !== 'string') {
            callback(new Error('boardID parameter should be a string!'));
        }
        else if (typeof filepath !== 'string') {
            callback(new Error('filepath parameter should be a string!'));
        } else {
            let errorMessage;

            fs.readFile(filepath, 'utf-8', (readError, listsString) => {
                if (readError != undefined) {
                    errorMessage = readError;
                }
                else {
                    try {
                        let listsObject = JSON.parse(listsString);
                        let boardList = listsObject[boardID];
                        if (Array.isArray(boardList)) {
                            callback(null, boardList);
                        } else {
                            errorMessage = new Error('Wrong Output');
                        }
                    }
                    catch (parseError) {
                        errorMessage = parseError;
                    }
                }
                if (errorMessage != undefined) {
                    callback(errorMessage);
                }
            })
        }
    }, 2 * 1000);
}