let callback6 = require('../callback6');
let cb = require('./cb');
const path = require('path');


let boardFilePath = path.join(__dirname, '../data/boards.json');
let listfilepath = path.join(__dirname, '../data/lists.json');
let cardsfilepath = path.join(__dirname, '../data/cards.json');


callback6("mcu453ed", cb, {
    board: boardFilePath,
    card: cardsfilepath,
    list: listfilepath,
});

//Test for no parameter passed
callback6();

//Test for skipping parameter
callback6("mcu453ed", cb);

//Test for passing wrong paths in path object
callback6("mcu453ed", cb, { board: 'asdasdfd' });
callback6("mcu453ed", cb, { board: 'asdasdfd', card: 'safsfdfvadv' });
callback6("mcu453ed", cb, { board: 'asdasdfd', card: 'safsfdfvadv', list: 'dckdgjadcjacj' });