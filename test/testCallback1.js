let callback1 = require('../callback1');
let cb = require('./cb');
const path = require('path');


//Test for Everything Right passing only 1 id
let filepath = path.join(__dirname, '../data/boards.json');
callback1("mcu453ed", cb, filepath);

//Test for Everything Right passing only multiple ids
filepath = path.join(__dirname, '../data/boards.json');
callback1(["mcu453ed", "abc122dc"], cb, filepath);

//Tests for passing no parameter
callback1();
callback1("mcu453ed", cb);
callback1("mcu453ed");


//Tests for wrong parameter types

//1. Filepath
callback1("mcu453ed", cb, 1235);

//2.Function
callback1("mcu453ed", 1235, filepath);

//3.ID
callback1(1234, cb, filepath);

//Test for wrong filepath
filepath = path.join(__dirname, '../data/b.json');
callback1("mcu453ed", cb, filepath);

//Test for wrong json
filepath = path.join(__dirname, '../data/testjsonfail.json');
callback1("mcu453ed", cb, filepath);

//Test for json not array
filepath = path.join(__dirname, '../data/testarrayfail.json');
callback1("mcu453ed", cb, filepath);

//Test for Data not found
filepath = path.join(__dirname, '../data/boards.json');
callback1("123465", cb, filepath);

//Test for some data not found
filepath = path.join(__dirname, '../data/boards.json');
callback1(["mcu453ed", "abc12"], cb, filepath);