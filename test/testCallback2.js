let callback2 = require('../callback2');
let cb = require('./cb');
const path = require('path');

//Test for Everything Right
let filepath = path.join(__dirname, '../data/lists.json');
callback2("mcu453ed", cb, filepath);


//Tests for passing no parameter
callback2();
callback2("mcu453ed", cb);
callback2("mcu453ed");


//Tests for wrong parameter types

//1. Filepath
callback2("mcu453ed", cb, 1235);

//2.Function
callback2("mcu453ed", 1235, filepath);

//3.ID
callback2(1234, cb, filepath);

//Test for wrong filepath
filepath = path.join(__dirname, '../data/b.json');
callback2("mcu453ed", cb, filepath);

//Test for wrong json
filepath = path.join(__dirname, '../data/testjsonfail.json');
callback2("mcu453ed", cb, filepath);

//Test for json not array
filepath = path.join(__dirname, '../data/testarrayfail.json');
callback2("mcu453ed", cb, filepath);

//Test for Data not found
filepath = path.join(__dirname, '../data/lists.json');
callback2("123465", cb, filepath);