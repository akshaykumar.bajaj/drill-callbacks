let callback4 = require('../callback4');
let cb = require('./cb');
const path = require('path');

let boardFilePath = path.join(__dirname, '../data/boards.json');
let listfilepath = path.join(__dirname, '../data/lists.json');
let cardsfilepath = path.join(__dirname, '../data/cards.json');


//Test for Everything Right
callback4("mcu453ed", cb, {
    board: boardFilePath,
    card: cardsfilepath,
    list: listfilepath,
});

//Test for no parameter passed
callback4();

//Test for skipping parameter
callback4("mcu453ed", cb);

//Test for passing wrong paths in path object
callback4("mcu453ed", cb, { board: 'asdasdfd' });
callback4("mcu453ed", cb, { board: 'asdasdfd', card: 'safsfdfvadv' });
callback4("mcu453ed", cb, { board: 'asdasdfd', card: 'safsfdfvadv', list: 'dckdgjadcjacj' });
