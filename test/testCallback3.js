let callback3 = require('../callback3');
let cb = require('./cb');
const path = require('path');


//Test for Everything Right
let filepath = path.join(__dirname, '../data/cards.json');
callback3("qwsa221", cb, filepath);


//Tests for passing no parameter
callback3();
callback3("qwsa221", cb);
callback3("qwsa221");


//Tests for wrong parameter types

//1. Filepath
callback3("qwsa221", cb, 1235);

//2.Function
callback3("qwsa221", 1235, filepath);

//3.ID
callback3(1234, cb, filepath);

//Test for wrong filepath
filepath = path.join(__dirname, '../data/b.json');
callback3("qwsa221", cb, filepath);

//Test for wrong json
filepath = path.join(__dirname, '../data/testjsonfail.json');
callback3("qwsa221", cb, filepath);

//Test for json not array
filepath = path.join(__dirname, '../data/testarrayfail.json');
callback3("qwsa221", cb, filepath);

//Test for Data not found
filepath = path.join(__dirname, '../data/cards.json');
callback3("123465", cb, filepath);