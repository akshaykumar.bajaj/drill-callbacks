/* 
    Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

let getBoardInfo = require('./callback1');
let getBoardLists = require('./callback2');
let getListCards = require('./callback3');

module.exports = function callback5(id, callback, filepath) {
    setTimeout(() => {
        try {
            getBoardInfo(id, (error, data) => {
                if (error) {
                    callback(error);
                } else {

                    getBoardLists(data[0].id, (error, lists) => {
                        if (error) {
                            callback(error);
                        } else {
                            let listIDs = lists.map(list => list.id);

                            getListCards(listIDs, callback, filepath.card);
                        }
                    }, filepath.list)
                }
            }, filepath.board);
        } catch (error) {
            if (error instanceof TypeError) {
                console.log(error);
            } else {
                callback(error);
            }
        }
    }, 2 * 1000);
}