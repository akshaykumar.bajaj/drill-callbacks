/* 
    Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data 
    in cards.json. Then pass control back to the code that called it by using a callback function.
*/
const fs = require('fs');

module.exports = function callback3(listIDs, callback, filepath) {
    setTimeout(() => {
        if (typeof callback !== 'function') {
            console.error(new Error('callback parameter should be a function!'));
        }
        else if (typeof listIDs !== 'string' && !Array.isArray(listIDs)) {
            callback(new Error('listID parameter should be a string or an array!'));
        }
        else if (typeof filepath !== 'string') {
            callback(new Error('filepath parameter should be a string!'));
        } else {
            fs.readFile(filepath, 'utf-8', (readError, cardsString) => {

                let errorMessage = undefined;

                if (readError) {
                    errorMessage = readError;
                }
                else {
                    try {

                        let cardsObject = JSON.parse(cardsString);
                        let cards;

                        if (typeof listIDs === 'string') {
                            cards = cardsObject[listIDs];
                            if (cards === undefined){
                                callback("Card not found for ID :" + listIDs);
                            } else {
                            callback(null, cards);
                       }
                     } else {
                            cards = listIDs.map(listID => {
                                let card = cardsObject[listID]
                                if (card != undefined) {
                                    return card;
                                } else {
                                    let message = "Card not found for ID :" + listID;
                                    return message;
                                }
                            });
                            callback(null, cards);
                        }
                    }
                    catch (parseError) {
                        errorMessage = parseError;
                    }
                }
                if (errorMessage != undefined) {
                    callback(errorMessage);
                }
            });
        }
    }, 2 * 1000);
}